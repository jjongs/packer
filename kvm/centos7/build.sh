#!/bin/bash

## Command Option Check
if [ $# -eq 0 ]
then
        echo "Usage: $0 VM_NAME";
        exit 1;
fi

OUTPUT_DIR="output_img"

DATE=`date +%Y%m%d`

packer build \
    -var "name=$1" \
    centos7.json

sudo virt-sparsify --format qcow2 --compress $(pwd)/$OUTPUT_DIR/$1 $(pwd)/$OUTPUT_DIR/$1_golden-image-$DATE.qcow2
