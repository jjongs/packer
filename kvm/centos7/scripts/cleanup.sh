#!/bin/bash

echo "debug: Executing scripts/cleanup.sh"
yum -y update

dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY
sync

yum clean all

rm -f /etc/udev/rules.d/70-persistent-net.rules
echo "--------------------------------"
