#!/bin/bash

# ulimit setting
sed -i 's/^*/#*/' /etc/security/limits.d/20-nproc.conf

cat >> /etc/security/limits.conf <<'EOF'

*                -       nofile         8192
*                -       nproc          8192
EOF

# kernel parameter
cat >> /etc/sysctl.conf <<'EOF'

net.ipv4.ipfrag_time = 15
net.ipv4.tcp_max_syn_backlog = 8192
net.ipv4.tcp_retries2 = 7
net.ipv4.tcp_fin_timeout = 20
net.ipv4.tcp_keepalive_probes = 2
net.ipv4.tcp_keepalive_time = 60
net.ipv4.tcp_keepalive_intvl = 10
EOF

# set hisrory
echo 'HISTTIMEFORMAT="%Y/%m/%d %T "' >> /etc/profile
echo 'export HISTTIMEFORMAT' >> /etc/profile
sed -i '/^HISTSIZE/c HISTSIZE=1000000' /etc/profile

# set crond without mail
sed -i "/^CRONDARGS\=/c CRONDARGS\='-m off'" /etc/sysconfig/crond
